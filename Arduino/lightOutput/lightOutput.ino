/* ====== ESP8266 Demo ======
 * Print out analog values
 * (Updated Dec 14, 2014)
 * ==========================
 *
 * Change SSID and PASS to match your WiFi settings.
 * The IP address is displayed to soft serial upon successful connection.
 *
 * Ray Wang @ Rayshobby LLC
 * http://rayshobby.net/?p=9734
 */

// comment this part out if not using LCD debug
#include <SoftwareSerial.h>

SoftwareSerial esp(2,3);

#define BUFFER_SIZE 512

int LED[4]={5,6,9,11};

int lPower[4]={0,0,0,0};

#define SSID  "Samuel Phone"      // change this to match your WiFi SSID
#define PASS  "81607352"  // change this to match your WiFi password
#define PORT  "8080"           // using port 8080 by default

char buffer[BUFFER_SIZE];

/*
// If using Software Serial for debug
// Use the definitions below
//#include <SoftwareSerial.h>
//SoftwareSerial dbg(7,8);  // use pins 7, 8 for software serial 
//#define esp Serial
*/

// If your MCU has dual USARTs (e.g. ATmega644)
// Use the definitions below
#define dbg Serial    // use Serial for debug

// By default we are looking for OK\r\n
char OKrn[] = "OK\r\n";
byte wait_for_esp_response(int timeout, char* term=OKrn) {
  unsigned long t=millis();
  bool found=false;
  int i=0;
  int len=strlen(term);
  // wait for at most timeout milliseconds
  // or if OK\r\n is found
  while(millis()<t+timeout) {
    if(esp.available()) {
      buffer[i++]=esp.read();
      if(i>=len) {
        if(strncmp(buffer+i-len, term, len)==0) {
          found=true;
          break;
        }
      }
    }
  }
  buffer[i]=0;
  dbg.print(buffer);
  return found;
}

void setup() {

  // assume esp8266 operates at 115200 baud rate
  // change if necessary to match your modules' baud rate
  esp.begin(9600);
  
  dbg.begin(9600);
  dbg.println("begin.");
    
  setupWiFi();

  // print device IP address
  dbg.print("device ip addr:");
  esp.println("AT+CIFSR");
  pinMode(LED[1], OUTPUT);
  pinMode(LED[2], OUTPUT);
  pinMode(LED[3], OUTPUT);
  pinMode(LED[4], OUTPUT);
  wait_for_esp_response(1000);
}

bool read_till_eol() {
  static int i=0;
  if(esp.available()) {
    buffer[i++]=esp.read();
    if(i==BUFFER_SIZE)  i=0;
    if(i>1 && buffer[i-2]==13 && buffer[i-1]==10) {
      buffer[i]=0;
      i=0;
      dbg.print(buffer);
      return true;
    }
  }
  return false;
}

void loop() {
  dbg.println("New Start");
  esp.println("AT+CIPSTART=0,\"TCP\",\"128.199.106.185\",80");
  wait_for_esp_response(1000);
  esp.println("AT+CIPSTATUS");
  wait_for_esp_response(1000);
  esp.println("AT+CIPMUX=1");
  wait_for_esp_response(1000);
  
  char msg[]="GET /power.php HTTP/1.0\r\n\r\n";
  esp.print("AT+CIPSEND=0,");
  esp.println(sizeof(msg));
  wait_for_esp_response(2000);
  esp.println(msg);
  if (wait_for_esp_response(5000))
  {
    wait_for_esp_response(1000);
    wait_for_esp_response(5000);
    dbg.println("Finish");
    
   
     
     int post=0;
     dbg.println("Magic!");
     dbg.println(buffer);
     dbg.println(strlen(buffer));
     while (strncmp(buffer+post-2,"A\n",2)!=0)
     {
        post++;
     }

     char lPowers[4][5]={""};
     int lCount=0;
     int sCount=0;
     while (post<strlen(buffer))
     {
        if (lCount<4)
        {
          if (buffer[post]!='\n')
          {
            lPowers[lCount][sCount]=buffer[post];
            sCount++;
          }
          else
          {
            lPowers[lCount][sCount]=0;
            lPower[lCount]=atoi(lPowers[lCount]);
            dbg.print("\n\nPower 1:");
            dbg.println(lPower[lCount]);
            dbg.println(lPowers[lCount]);
            analogWrite(LED[lCount],lPower[lCount]*255/100);
            lCount++;
            sCount=0;
          }
        }
        dbg.print(buffer[post]);
        post++;
     }
     
     dbg.println(post);
     memcpy(buffer,0,sizeof(buffer)); 
  }
}


void setupWiFi() {
  // try empty AT command
  esp.println("AT");
  wait_for_esp_response(1000);

  // set mode 1 (client)
  esp.println("AT+CWMODE=1");
  wait_for_esp_response(1000);  

  // reset WiFi module
  esp.print("AT+RST\r\n");
  wait_for_esp_response(1500);
  delay(3000);
 
  // join AP
  esp.print("AT+CWJAP=\"");
  esp.print(SSID);
  esp.print("\",\"");
  esp.print(PASS);
  esp.println("\"");
  // this may take a while, so wait for 5 seconds
  wait_for_esp_response(5000);
  
  esp.println("AT+CIPSTO=30");  
  wait_for_esp_response(1000);

  // start server
  esp.println("AT+CIPMUX=1");
  wait_for_esp_response(1000);
  
  
    
}
